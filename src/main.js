// const data = ['Regina', 'Napolitaine', 'Spicy'];
// let name, html, url;
// let res = ``;
// data.forEach(element => { 
//     name = element;
//     url = `images/${name.toLowerCase()}.jpg`
//     console.log(url)
//     html = `<article class="pizzaThumbnail">
//                 <a href ="${url}">
//                     <img src="${url}"/>
//                     <section>${name}</section> 
//                 </a>
//             </article>`
//     console.log(html);
//     res = res + html;
// });
// document.querySelector('.pageContent').innerHTML = res;
// const htmlData = data.map(element => {
	//     url = `images/${element.toLowerCase()}.jpg`
//     element = `<article class="pizzaThumbnail">
//                 <a href ="${url}">
//                     <img src="${url}"/>
//                     <section>${element}</section> 
//                 </a>
//             </article>`
//     return element;
// });
// console.log(htmlData);
// document.querySelector('.pageContent').innerHTML = htmlData.join('');


const data = [
	{
		name: 'Regina',
		base: 'tomate',
		price_small: 6.5,
		price_large: 9.95,
		image: 'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300'
	},
	{
		name: 'Napolitaine',
		base: 'tomate',
		price_small: 6.5,
		price_large: 8.95,
		image: 'https://images.unsplash.com/photo-1562707666-0ef112b353e0?&fit=crop&w=500&h=300'
	},
	{
		name: 'Spicy',
		base: 'crème',
		price_small: 5.5,
		price_large: 8,
		image: 'https://images.unsplash.com/photo-1458642849426-cfb724f15ef7?fit=crop&w=500&h=300',
	}
];


//alphabétique
// data.sort((a, b) => {
// if(a.name < b.name) {
// 	return -1;
// }
// else if(a.name > b.name) {
// 	return 1;
// }
// return 0;
// });

//Petit format croissant
// data.sort((a,b) => {
// 	if(a.price_small < b.price_small) {
// 		return -1;
// 	}
// 	else if(a.price_small > b.price_small) {
// 		return 1;
// 	}
// 	return 0;
// });

//Petit format croissant puis grand format croissant
data.sort((a,b) => {
	if(a.price_small < b.price_small) {
		return -1;
	}
	else if(a.price_small > b.price_small) {
		return 1;
	}
	else if(a.price_large < b.price_large) {
		return -1;
	}
	else if(a.price_large < b.price_large) {
		return 1;
	}
	return 0;
});

//filtre tomate
// const newData = data.filter(({base}) => base === 'tomate');

// filtre < 6€
// const newData = data.filter(({price_small}) => price_small < 6);

//filtre i
const newData = data.filter(({name}) => {
	var letter_Count = 0;
 	for (let i = 0; i < name.length; i++) 
 	{
    	if (name.charAt(i) === 'i') 
     	{
      		letter_Count += 1;
      	}
  	}
  	return letter_Count == 2;
});


let res = ``;
let url = ``;
let html = ``;

newData.forEach(({image, price_small, price_large}) => { 
	url = `${image}`;
    html = `<article class="pizzaThumbnail">
				<a href ="${url}">
					<img src="${url}"/>
						<section>
							<h4>${name}</h4>
							<ul>
                            	<li>Prix petit format : ${price_small.toFixed(2)} €</li>
                            	<li>Prix grand format : ${price_large.toFixed(2)} €</li>
							</ul>
						</section> 
                </a>
			</article>`
	res = res + html;
});
document.querySelector('.pageContent').innerHTML = res;
			